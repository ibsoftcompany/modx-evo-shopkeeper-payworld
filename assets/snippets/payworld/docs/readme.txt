MODX Evolution + Shopkeeper

Инструкция по подключению к платежному сервису Payworld (Мир платежей):

1. Скопируйте файлы сниппета в папку assets/snippets/payworld/

2. В системе управления перейдите "Элементы" -> "Управление элементами" 
-> "Сниппеты". Нажмите ссылку "Новый сниппет".

Название сниппета: Payworld, 
Описание: Оплата через Мир платежей, 
Откройте файл payworld_snippet.txt, скопируйте содержимое и вставьте в 
поле "Код сниппета". Нажмите кнопку "Сохранить". 

4. В системе управления откройте для редактирования страницу, которая 
открывается после оформления заказа (&gotoid в eForm). 
Вставьте в поле "Содержимое ресурса" вызов сниппета: 

[!Payworld!] 

Нажмите Сохранить.

5. Проверьте, чтобы на странице оформления заказа в вызове сниппета 
eForm был указан параметр &gotoid. 

Пример: 

[!eForm? &gotoid=`7` &formid=`shopOrderForm` &tpl=`shopOrderForm` 
&report=`shopOrderReport` &subject=`Заказ` 
&eFormOnBeforeMailSent=`populateOrderData` 
&eFormOnMailSent=`sendOrderToManager`!] 

Здесь 7 - это ID страницы, которая будет открываться после отправки 
заказа. 

6. В шаблоне формы должен быть выпадающий список для выбора метода
оплаты. 

Пример: 

   <select name="payment" >
      <option value="При получении">При получении</option>
      <option value="webmoney">WebMoney</option>
      <option value="robokassa">Другие электронные деньги</option>
   </select>

Добавьте строку <option value="payworld">Мир платежей</option>. 
Должно выглядеть, например, так: 

   <select name="payment" >
      <option value="При получении">При получении</option>
      <option value="payworld">Мир платежей</option>
      <option value="webmoney">WebMoney</option>
      <option value="robokassa">Другие электронные деньги</option>
   </select>
   
Теперь после отправки заказа на следующей странице будет появляться 
кнопка для перехода к оплате. 

6. Настройте модуль оплаты Payworld: 

SERVER - Адрес страницы оплаты в системе "Мир платежей"
SELLER_NAME - Идентификатор продавца в системе "Мир платежей"
SHOP_ID - Идентификатор магазина в системе "Мир платежей"
SECRET_CODE - Секретный код магазина в системе "Мир платежей"

BUTTON_TEXT - Текст на кнопке перехода к оплате
EMAIL_ON_ERROR - E-mail адрес(а), на который(ые) следует отправлять 
    уведомления в случае:
        - попытки подмены результатов платежа (неверный хэш)
        - если пришел платеж на заказ, номер которого не найден.
    Примеры:    
        user@example.com
        user@example.com, anotheruser@example.com
        User <user@example.com>
        User <user@example.com>, Another User <anotheruser@example.com>
    или оставьте пустым для отключения данной функции

7. Зайдите в ваш акаунт в платежном сервисе Мир платежей и перейдите 
в раздел редактирования данных магазина. 

В настройках введите: 

    URL:
    http://ваш_сайт

    Result url:
    http://ваш_сайт/index.php?id=ID_страницы_заказа&action=callback

    Success url:
    http://ваш_сайт/index.php?id=ID_страницы_заказа&action=success

    Fail url:
    http://ваш_сайт/index.php?id=ID_страницы_заказа&action=fail
